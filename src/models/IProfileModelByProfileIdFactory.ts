import IProfileModel from './IProfileModel'

export default interface IProfileModelByProfileIdFactory {
  create(profileId: string): IProfileModel
}
