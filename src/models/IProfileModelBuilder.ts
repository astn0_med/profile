import IProfileModel from './IProfileModel'

export default interface IProfileModelBuilder {
  addProfileId(ProfileId: string): IProfileModelBuilder
  addLogin(login: string): IProfileModelBuilder
  addPassword(password: string): IProfileModelBuilder
  create(): IProfileModel
}
