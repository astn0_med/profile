import IProfileModel from './IProfileModel'

export default interface IProfileModelByLoginFactory {
  create(login: string): IProfileModel
}
