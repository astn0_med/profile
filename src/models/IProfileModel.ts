import IProfileModelStruct from './IProfileModelStruct'

export default interface IProfileModel {
  handle(): Promise<IProfileModelStruct>
}
