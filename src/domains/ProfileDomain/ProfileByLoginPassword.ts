import IProfileModelByLoginFactory from '../../models/IProfileModelByLoginFactory'
import IProfileModelByProfileIdFactory from '../../models/IProfileModelByProfileIdFactory'
import IProfileModelStruct from '../../models/IProfileModelStruct'
import IProfileDomain from './IProfileDomain'
import IProfileDomainStruct from './IProfileDomainStruct'
import {
  ProfileDomainAdapter,
  ProfileDomainAdapterCreator,
} from './ProfileDomainAdapter'

export default class ProfileByLoginPassword implements IProfileDomain {
  private readonly _login: string
  private readonly _password: string
  private readonly _profileModelByLoginFactory: IProfileModelByLoginFactory
  private readonly _profileDomainAdapterCreator: ProfileDomainAdapterCreator
  constructor(
    login: string,
    password: string,
    profileModelByLoginFactory: IProfileModelByLoginFactory,
    profileDomainAdapterCreator: ProfileDomainAdapterCreator
  ) {
    this._login = login
    this._password = password
    this._profileDomainAdapterCreator = profileDomainAdapterCreator
    this._profileModelByLoginFactory = profileModelByLoginFactory
  }

  public async handle(): Promise<IProfileDomainStruct> {
    // todo: загрузи по логину профайл из бд
    const profileModel = this._profileModelByLoginFactory.create(this._login)
    const modelProfileResult: IProfileModelStruct = await profileModel.handle()

    const profileDomainAdapter: ProfileDomainAdapter = this._profileDomainAdapterCreator.create(
      modelProfileResult
    )
    const result: IProfileDomainStruct = profileDomainAdapter.map()

    // todo: Сравни пароли, выбрось ошибку, если не верный
    // todo: получи токен пользователя и добавь его в result
    return result
  }
}
