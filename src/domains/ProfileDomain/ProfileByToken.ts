import IProfileModelBuilder from '../../models/IProfileModelBuilder'
import IProfileModelByProfileIdFactory from '../../models/IProfileModelByProfileIdFactory'
import IProfileModelStruct from '../../models/IProfileModelStruct'
import ITokenService from '../../services/TokenService/ITokenService'
import ITokenServiceFabric from '../../services/TokenService/ITokenServiceFabric'
import IProfileDomain from './IProfileDomain'
import IProfileDomainStruct from './IProfileDomainStruct'
import {
  ProfileDomainAdapterCreator,
  ProfileDomainAdapter,
} from './ProfileDomainAdapter'

/**
 * Класс для получения профайла по profileID
 */
export default class ProfileByToken implements IProfileDomain {
  private readonly _token: string
  private _profileId: string

  private readonly _profileModelByProfileIdFactory: IProfileModelByProfileIdFactory
  private readonly _profileDomainAdapterCreator: ProfileDomainAdapterCreator
  private readonly _tokenServiceFabric: ITokenServiceFabric

  constructor(
    token: string,
    profileModelByProfileIdFactory: IProfileModelByProfileIdFactory,
    profileDomainAdapterCreator: ProfileDomainAdapterCreator,
    tokenServiceFabric: ITokenServiceFabric
  ) {
    this._profileId = ''
    this._token = token
    this._profileModelByProfileIdFactory = profileModelByProfileIdFactory
    this._profileDomainAdapterCreator = profileDomainAdapterCreator
    this._tokenServiceFabric = tokenServiceFabric
  }

  public async handle(): Promise<IProfileDomainStruct> {
    const tokenService: ITokenService = this._tokenServiceFabric.create(
      this._token
    )
    this._profileId = await tokenService.check()

    const profileModel = this._profileModelByProfileIdFactory.create(
      this._profileId
    )
    const modelProfileResult: IProfileModelStruct = await profileModel.handle()

    const profileDomainAdapter: ProfileDomainAdapter = this._profileDomainAdapterCreator.create(
      modelProfileResult
    )
    const result: IProfileDomainStruct = profileDomainAdapter.map()

    result.token = this._token

    return result
  }
}
