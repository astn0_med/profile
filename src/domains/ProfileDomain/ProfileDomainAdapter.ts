import IProfileModelStruct from '../../models/IProfileModelStruct'
import IProfileDomainStruct from './IProfileDomainStruct'

export class ProfileDomainAdapterCreator {
  create(data: IProfileModelStruct) {
    return new ProfileDomainAdapter(data)
  }
}

export class ProfileDomainAdapter {
  private readonly _data: IProfileModelStruct
  constructor(data: IProfileModelStruct) {
    this._data = data
  }

  map(): IProfileDomainStruct {
    return {
      login: this._data.login,
      token: '',
    }
  }
}
