import IProfileDomainStruct from './IProfileDomainStruct'

export default interface IProfileDomain {
  handle(): Promise<IProfileDomainStruct>
}
