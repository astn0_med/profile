export default interface ITokenService {
  check(): Promise<string>
}
