import ITokenService from './ITokenService'

export default interface ITokenServiceFabric {
  create(token: string): ITokenService
}
