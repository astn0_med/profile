import IProfileModel from '../src/models/IProfileModel'
import profileModelByProfileIdFactory from '../src/models/IProfileModelByProfileIdFactory'
import IProfileModelStruct from '../src/models/IProfileModelStruct'
import ITokenService from '../src/services/TokenService/ITokenService'
import ITokenServiceFabric from '../src/services/TokenService/ITokenServiceFabric'
import ProfileByToken from '../src/domains/ProfileDomain/ProfileByToken'
import { ProfileDomainAdapterCreator } from '../src/domains/ProfileDomain/ProfileDomainAdapter'
import IProfileDomainStruct from '../src/domains/ProfileDomain/IProfileDomainStruct'

class Model implements IProfileModel {
  constructor(profileId) {}
  public async handle(): Promise<IProfileModelStruct> {
    return {
      profileId: '1',
      login: 'test',
      password: '12345',
    }
  }
}

class modelFactory implements profileModelByProfileIdFactory {
  public create(profileId: string): IProfileModel {
    return new Model(profileId)
  }
}

class Token implements ITokenService {
  constructor(token) {}

  public async check(): Promise<string> {
    return 'profileId'
  }
}

class TokenFabric implements ITokenServiceFabric {
  public create(token: string): ITokenService {
    return new Token(token)
  }
}

it('get profile by token', async () => {
  const profile: ProfileByToken = new ProfileByToken(
    'token',
    new modelFactory(),
    new ProfileDomainAdapterCreator(),
    new TokenFabric()
  )
  const result: IProfileDomainStruct = await profile.handle()

  console.log('result = ', result)

  expect(true).toEqual(true)
})
